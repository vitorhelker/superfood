import React from "react";
import { Route, Switch } from "react-router-dom";
import Index from "./pages/index";
import Place from "./pages/place";
import Checkout from "./pages/checkout";

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Index} />
    <Route path="/place/:slug" component={Place} />
    <Route path="/checkout" component={Checkout} />
  </Switch>
);

export default Routes;
