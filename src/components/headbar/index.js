import React from "react";
import {NavLink} from 'react-router-dom';

const Headbar = () => {
    return (
        <nav id="header" className="navbar navbar-expand-lg navbar-dark">
            
            <NavLink className="navbar-brand" style={{width: '100px !important'}} to="/">
                <img src="../images/vectorpaint_1.svg"/>
            </NavLink>

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link" href="#">achar um local<span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">criar conta</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">login</a>
                    </li>

                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Minha Conta
                         </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a className="dropdown-item" href="#">Action</a>
                            <a className="dropdown-item" href="#">Another action</a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>

                    <li className="nav-link" id="header-cart">
                        <i className="fas fa-shopping-cart"></i><span>1</span>
                    </li>
                    <button id="create-place">+ cadastrar estabelecimento</button>
                </ul>
            </div>
        </nav>
    )
}

export default Headbar;