import React from "react";

const Footer = () => {
    return (
        <div id="footer">
            <div className="row">
                <div className="col-md-4">
                    <h2>Quem somos</h2>
                    <ul>
                        <li><a href="/about">Sobre nós</a></li>
                        <li><a href="/institutional">Institucional</a></li>
                        <li><a href="">Nossas soluções</a></li>
                        <li><a href="">Entre em contato</a></li>
                    </ul>
                </div>
                <div className="col-md-4">
                    <h2>Clientes</h2>
                    <ul>
                        <li><a href="/terms">Termos e políticas</a></li>
                        <li><a href="/faq">Perguntas frequentes</a></li>
                        <li><a href="/meia_entrada">Meia entrada</a></li>
                    </ul>
                </div>

                <div className="col-md-4 text-right">
                    <img id="footer-logo" src="../images/symbol_white.svg" />
                    <a href="mailto:contato@contato.com.br">Contato@Contato.com.br</a>
                    <p>Redes Sociais</p>
                </div>
            </div>
            <hr></hr>
            <div id="copyright" className="text-center col-md-12">
                <p>Feito com Odio no Brasil para o mundo. © 2019</p>
            </div>
        </div>
    )
}

export default Footer;