import React, { useState } from 'react'
import axios from 'axios'

import './styles.css'

function UserPaymentCard({ callbackParent }){
  const [cpf, setCpf] = useState('')
  const [contactTel, setContactTel] = useState('')
  const [birthdate, setBirthdate] = useState('')
  const [cep, setCep] = useState('')
  const [street, setStreet] = useState('')
  const [number, setNumber] = useState('')
  const [neighborhood, setNeighborhood] = useState('')
  const [city, setCity] = useState('')
  const [state, setState] = useState('')

  const handleData = (id,value) => {
    callbackParent(id,value)
  }

  async function fetchCEP(cep) {
    setCep(cep)
    console.log("cp", cep, cep.length)
    if (cep && cep.length === 8) {
      await axios.get(`https://viacep.com.br/ws/${cep}/json/`, {
        method: "GET",
        mode: "cors",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        setCep(res.data.cep)
        setStreet(res.data.logradouro)
        setNeighborhood(res.data.bairro)
        setCity(res.data.localidade)
        setState(res.data.uf)
        handleData('address', res.data)
      })
      .catch(err => {console.log(err)})
    }
  }
  
  return (
    <div className="row">
      <div className="col-6 mb-2">
        <label htmlFor="cpf">CPF</label>
        <input
          id="cpf" 
          className="form-control" 
          onChange={(e)=>handleData(e.target.id,e.target.value)}
        />
      </div>
      <div className="col-6 mb-2">
        <label htmlFor="contactTel">Telefone para contato</label>
        <input  
          id="contactTel" 
          className="form-control" 
          onChange={(e)=>handleData(e.target.id,e.target.value)}
        />
      </div>
      <div className="col-6 mb-2">
        <label htmlFor="birthDate">Data de nascimento</label>
        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="fa fa-calendar" area-hidden="true"></i>
            </span>
          </div>
          <input  
            id="birthDate" 
            className="form-control form-control-md"
            placeholder="DD/MM/AA" 
            onChange={(e)=>handleData(e.target.id,e.target.value)}
          />
        </div>
      </div>
      <div className="col-6 mb-2">
        <label htmlFor="cep">CEP</label>
        <input 
          type="text" 
          id="cep" 
          className="form-control" 
          onChange={e=>fetchCEP(e.target.value)}
        />
      </div>
      <div className="col-8 mb-2">
        <label htmlFor="street">Endereço</label>
        <input 
          id="street" 
          className="form-control" 
          value={street}
          onChange={(e)=>handleData(e.target.id,e.target.value)}
        />
      </div>
      <div className="col-4 mb-2">
        <label htmlFor="number">Numero</label>
        <input  
          id="number" 
          className="form-control" 
          onChange={(e)=>handleData(e.target.id,e.target.value)}
        />
      </div>
      <div className="col-4 mb-2">
        <label htmlFor="neighborhood">Bairro</label>
        <input 
          id="neighborhood" 
          className="form-control" 
          value={neighborhood}
          onChange={(e)=>handleData(e.target.id,e.target.value)}
        />
      </div>
      <div className="col-4 mb-2">
        <label htmlFor="city">Cidade</label>
        <input 
          id="city" 
          className="form-control" 
          value={city}
          onChange={(e)=>handleData(e.target.id,e.target.value)}
        />
      </div>
      <div className="col-4 mb-2">
        <label htmlFor="state">Estato</label>
        <input 
          id="state" 
          className="form-control" 
          value={state}
          onChange={(e)=>handleData(e.target.id,e.target.value)}
        />
      </div>
    </div>
  )
}

export default UserPaymentCard