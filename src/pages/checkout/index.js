import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { Alert, Button } from "react-bootstrap";
import axios from "axios";

// import api from '../../services/api';
import "./styles.css";

import UserPaymentCard from "../../components/UserPaymentCard/Index";

const data = [
  { id: 2, nome: "Itaipava Go Draft", image: "./images/produto2.jpg" },
  { id: 6, nome: "Pizza Especial", image: "./images/produto3.jpg" },
];

export default function Index() {
  import(`moment/locale/${navigator.language.toLocaleLowerCase()}`).then();
  const history = useHistory();

  const [show, setShow] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const [order, setOrder] = useState({ orderItens: [], event: {} });
  const [paymentOptions, setPaymentOptions] = useState([
    { text: "Cartão de credito", value: "cartao" },
    { text: "Boleto", value: "boleto" },
  ]);

  const [cartItens, setCartItens] = useState([]);
  const [total, setTotal] = useState("");

  const [address, setAddress] = useState({
    cep: "",
    address: "",
    number: "",
    city: "",
    state: "",
    neighborhood: "",
    complement: "",
  });

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [telephone, setTelephone] = useState("");

  const [tickets, setTickets] = useState([{ partFname: "", partLname: "" }]);
  const [paymentType, setPaymentType] = useState("");

  const { token } = useParams();

  const updateAddress = (e) => {
    setAddress({
      ...address,
      [e.target.name]: e.target.value,
    });
  };

  const getCep = () => {
    axios
      .get(`https://viacep.com.br/ws/${address.cep}/json`)
      .then((res) => {
        setAddress({
          ...address,
          address: res.data.logradouro,
          state: res.data.uf,
          city: res.data.localidade,
          neighborhood: res.data.bairro,
          complement: res.data.complemento,
        });

        // var x = document.getElementById("invalid-cep");
        // if (!x.classList.contains("hide")) {
        //   x.classList.add("hide");
        //   x.classList.add("invalid-cep");
        //   x.classList.remove("invalid-cep-show");
        // }
      })
      .catch((err) => {
        setAddress({
          ...address,
          address: "",
          state: "",
          city: "",
          neighborhood: "",
          complement: "",
        });
        console.log(err);
        // var x = document.getElementById("invalid-cep");
        // if (x.classList.contains("hide")) {
        //   x.classList.remove("hide");
        //   x.classList.add("invalid-cep-show");
        //   x.classList.remove("invalid-cep");
        // }
      });
  };

  // useEffect(() => {
  //   async function getCart() {
  //     // const res = await api.get("/checkout/" + token)
  //     if (res.data) {
  //       let { order, tickets } = res.data;
  //       // setOrder(order)
  //       // setTickets(tickets)

  //       // if (order && order.event && order.event.banner) {
  //       //   setBanner(order.event.banner)
  //       // }

  //       // if (order.status === "finalizado") {
  //       //   history.push("/my_tickets")
  //       // } else if (order.status === "processando") {
  //       //   history.push("/confirmation/" + token)
  //       // }

  //       setLoading(false)
  //       //loggedUser.id === order.steward.id
  //       if(order.steward){
  //         if (order.steward.cash_sale) {
  //           setPaymentOptions([...paymentOptions, { text: "Dinheiro", value: "dinheiro" }])
  //         }
  //       }

  //       if (order.total_value === 0) {
  //         setIsSemCortesia(false)
  //       }
  //       tickets = []
  //     } else {
  //       setLoading(false)
  //       // let error = ErrorService.getErrorMessage(err);
  //       // this.error =
  //       //   error ||
  //       //   "Houve um problema ao tentar carregar seu pedido, favor tente novamente.";
  //     }

  //   }

  //   getCart()
  // }, [])

  // document.title = "Funz - " + event.name
  useEffect(() => {
    console.log(localStorage.getItem("itens"));
    setCartItens(JSON.parse(localStorage.getItem("itens")));
    setTotal(Number(localStorage.getItem("total")));
  }, []);

  function enviarMensagem() {
    var celular = "55" + telephone;

    var texto = `*Pedido online recebido:* \n  `;

    cartItens.map((item) => {
      texto = texto + `\n ${item.quantity} x ${item.nome}`;
    });

    texto = texto + `\n \n *Nome:* ${firstName} ${lastName}`;
    texto = texto + `\n \n *Forma de pagamento:*`;
    texto =
      texto +
      `\n \n *Endereço de Entrega:* ${address.address} ${address.number} ${address.complement}, ${address.state} - ${address.city}`;
    texto = window.encodeURIComponent(texto);

    // window.open("https://api.whatsapp.com/send?phone=" + celular + "&text=" + texto, "_blank
    window.open("https://wa.me/" + celular + "?text=" + texto, "_blank");
    //Obs.. use "_system", no lugar de blank, caso você esteja usando Phonegap / Cordova / Ionic ou qualquer um baseado em webview;
  }

  // function handleDataUser(id, value) {
  //   if (id === 'cpf') { setCpf(value) }
  //   else if (id === 'contactTel') { setContactTel(value) }
  //   else if (id === 'birthDate') { setBirthdate(value) }
  //   else if (id === 'number') { setNumber(value) }
  //   else if (id === 'address') {
  //     console.log("adrs", value)
  //     setCep(value.cep)
  //     setStreet(value.logradouro)
  //     setNeighborhood(value.bairro)
  //     setCity(value.localidade)
  //     setState(value.uf)
  //   }
  // }

  return (
    <>
      {/* <Alert show={show} variant="danger" className="fixed-top text-center">
        {errorMsg}
        <div className="float-right">
          <Button onClick={() => setShow(false)} variant="outline-danger">
            X
            </Button>
        </div>
      </Alert> */}
      {/* <div id="main-event">
        <div
          className="background-banner"
          style={{ backgroundImage: `url(${banner})` }}
        />
        <div
          className="container checkout-banner"
          style={{ backgroundImage: `url(${banner})` }}
        >
          <div className="share">
            <a
              href="https://www.facebook.com/share/"
              target="_blank"
              className="share-btn facebook"
            ></a>
            <a
              href="https://www.twitter.com/share/"
              target="_blank"
              className="share-btn twitter"
            ></a>
          </div>
        </div>
        <div className="event-desc container">
          <h1 className="color-primary">{order.event.name}</h1>
          <p><i className="fa fa-calendar"></i> {order.event.date}</p>
        </div>
      </div> */}

      <div className="checkout">
        <div className="container">
          <h1>Finalizar pedido</h1>
          <div className="row">
            <div className="col-md-12">
              <div className="panel">
                <div className="panel-header">
                  <h2>Resumo do pedido</h2>
                </div>
                <div className="panel-body">
                  <div className="row">
                    {cartItens.map((item, idx) => (
                      <React.Fragment key={idx}>
                        <div className="col-md-12">
                          <div className="panel-name">
                            <p>
                              {item.quantity}
                              &nbsp; x &nbsp;
                              {item.name}
                              &nbsp;
                              {item.price
                                ? item.price.toLocaleString("pt-BR", {
                                    style: "currency",
                                    currency: "BRL",
                                  })
                                : "R$ 0,00"}
                              &nbsp; = &nbsp;
                              {(item.price * item.quantity).toLocaleString(
                                "pt-BR",
                                {
                                  style: "currency",
                                  currency: "BRL",
                                }
                              )}
                            </p>
                          </div>
                        </div>

                        {/* <div className="col-12 col-sm-6 col-md-3">
                          <div className="panel-title">Quantidade</div>
                          <div className="panel-name">
                            {item.amount}
                          </div>
                        </div>
                        <div className="col-12 col-sm-6 col-md-3">
                          <div className="panel-title">Sub-Total</div>
                          <div className="panel-name">
                            {
                              item.total_value.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                              })
                            }
                          </div>
                        </div> */}
                      </React.Fragment>
                    ))}
                    <div className="col-md-12" style={{ fontWeight: "bold" }}>
                      Total do pedido:{" "}
                      {total
                        ? total.toLocaleString("pt-BR", {
                            style: "currency",
                            currency: "BRL",
                          })
                        : "R$ 0,00"}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-md-9 shadow-sm" id="checkout-login">
              <div className="col-md-2">
                <div className="checkout-account-icon">
                  <i className="fas fa-user"></i>
                </div>
              </div>
              <div className="col-md-10">
                <span>
                  <b>
                    Faça <a href="">login</a> ou <a href="">crie sua conta</a>{" "}
                    agora.
                  </b>
                </span>
                <p>
                  Tenha acesso ao seu histórico pedidos e facilite seus próximos
                  pedidos.
                  <br />
                  Caso não queira, basta preencher seus dados abaixo.
                </p>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-md-12">
              <div className="panel">
                <div className="panel-header">
                  <h2>1. Dados do cliente</h2>
                </div>
                <div className="panel-body" id="recebimento">
                  <div className="row">
                    <div className="col-md-3 mb-2">
                      <label htmlFor="buyerFname">Nome</label>
                      <input
                        value={firstName}
                        name="buyerFname"
                        id="buyerFname"
                        className="form-control"
                        onChange={(e) => setFirstName(e.target.value)}
                      />
                    </div>
                    <div className="col-md-3 mb-2">
                      <label htmlFor="buyerLname">Sobrenome</label>
                      <input
                        value={lastName}
                        name="buyerLname"
                        id="buyerLname"
                        className="form-control"
                        onChange={(e) => setLastName(e.target.value)}
                      />
                    </div>
                    <div className="col-md-6 mb-2">
                      <label htmlFor="telephone">Telefone</label>
                      <input
                        value={telephone}
                        name="telephone"
                        id="telephone"
                        className="form-control"
                        onChange={(e) => setTelephone(e.target.value)}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-6 mb-2">
                      <label htmlFor="buyerMail">E-mail</label>
                      <input
                        type="email"
                        value={email}
                        name="buyerMail"
                        id="buyerMail"
                        className="form-control"
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </div>
                    <div className="col-md-6 mb-2">
                      <label htmlFor="buyerMail"> Confirmar e-mail</label>
                      <input
                        type="email"
                        name="buyerMail"
                        id="buyerMail"
                        className="form-control"
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-md-12">
              <div className="panel">
                <div className="panel-header">
                  <div className="row">
                    <div className="col-md-12">
                      <h2>2. Endereço de Entrega</h2>
                    </div>
                  </div>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-md-4">
                      <label htmlFor="cep">CEP</label>
                      <input
                        className="form-control"
                        onChange={updateAddress}
                        onBlur={getCep}
                        value={address.cep}
                        name="cep"
                      />
                    </div>
                    <div className="col-md-2">
                      <label htmlFor="number">Numero</label>
                      <input
                        className="form-control"
                        onChange={updateAddress}
                        value={address.number}
                        name="number"
                      />
                    </div>
                    <div className="col-md-6">
                      <label htmlFor="complement">
                        Complemento / Referência
                      </label>
                      <input
                        className="form-control"
                        onChange={updateAddress}
                        value={address.complement}
                        name="complement"
                      />
                    </div>
                  </div>
                  <div className="row mt-2">
                    <div className="col-md-4">
                      <label htmlFor="address">Endereço</label>
                      <input
                        className="form-control"
                        onChange={updateAddress}
                        value={address.address}
                        name="address"
                      />
                    </div>
                    <div className="col-md-2">
                      <label htmlFor="neighborhood">Bairro</label>
                      <input
                        className="form-control"
                        onChange={updateAddress}
                        value={address.neighborhood}
                        name="neighborhood"
                      />
                    </div>
                    <div className="col-md-4">
                      <label htmlFor="city">Cidade</label>
                      <input
                        className="form-control"
                        onChange={updateAddress}
                        value={address.city}
                        name="city"
                      />
                    </div>
                    <div className="col-md-2">
                      <label htmlFor="state">Estado</label>
                      <input
                        className="form-control"
                        onChange={updateAddress}
                        value={address.state}
                        name="state"
                      />
                    </div>

                    <div className="col-md-6" id="checkout-address-save">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="gridCheck"
                      />
                      <label className="form-check-label" for="gridCheck">
                        Salvar endereço para futuras compras
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-md-12">
              <div className="panel">
                <div className="panel-header">
                  <h2>3. Dados do pagamento</h2>
                </div>
                <div className="panel-body" id="payment-method">
                  <div className="row payment-type">
                    <div className="col-sm-2 mb-2">
                      <label>
                        <input type="radio" name="optradio" checked /> Dinheiro
                      </label>
                    </div>
                    <div className="col-sm-2 mb-2">
                      <label>
                        <input type="radio" name="optradio" checked /> Débito -
                        Visa
                      </label>
                    </div>
                    <div className="col-sm-2 mb-2">
                      <label>
                        <input type="radio" name="optradio" checked /> Débito -
                        Mastercard
                      </label>
                    </div>
                    <div className="col-sm-2 mb-2">
                      <label>
                        <input type="radio" name="optradio" checked /> Crédito -
                        Visa
                      </label>
                    </div>
                    <div className="col-sm-2 mb-2">
                      <label>
                        <input type="radio" name="optradio" checked /> Crédito -
                        Mastercard
                      </label>
                    </div>
                    <div className="col-sm-2 mb-2">
                      <label>
                        <input type="radio" name="optradio" checked /> Vale
                        Refeição
                      </label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6 mb-2">
                      <label htmlFor="telephone">CPF ou CNPJ na nota?</label>
                      <input
                        value={telephone}
                        name="telephone"
                        id="telephone"
                        className="form-control"
                        onChange={(e) => setTelephone(e.target.value)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row mt-4">
            <div className="col-md-6">
              <button className="btn btn-whats" onClick={enviarMensagem}>
                Finalizar e enviar pedido via whatsapp
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
