import React, { useEffect, useState } from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Api from "../../services/api";

/* Scroll Functon */
window.onresize = function () {
  let width = window.innerWidth > 0;
  let height = window.innerHeight > 0;

  if (height > 100) {
    let clear = document.getElementByClassName("btn-fixed");
    clear.removeClass("btn-fixed").addClass("btn-fixed2");
  }
};

const Place = (props) => {
  const history = useHistory();

  const values = [{
    nome: 'RedBull Tropical',
    image: '../../images/produto1.jpg',
    desc: 'RedBull Tropical 350ml',
    price: 18.90,
    type: 2,
    quantity: 0
  },
  {
    nome: 'Itaipava Go Draft',
    image: '../../images/produto2.jpg',
    desc: 'Itaipava Gro Draft',
    price: 18.90,
    type: 2,
    quantity: 0
  },
  {
    nome: 'Pizza Especial',
    image: '../../images/produto3.jpg',
    desc: 'Pizza Especial Sabor Tristeza',
    price: 18.90,
    type: 1,
    quantity: 0
  },
  {
    nome: 'Guaraná Lata',
    image: '../../images/produto4.jpg',
    desc: 'Guaraná Lata',
    price: 18.90,
    type: 2,
    quantity: 0
  },


  // {
  //   nome: 'Itaipava Go Draft',
  //   image: '../../images/produto4.jpg',
  //   desc: 'Itaipava Go Graft',
  //   price: 18.90,
  //   type: 2,
  //   quantity: 0
  // },
  // {
  //   nome: 'Pizza Especial',
  //   image: '../../images/produto3.jpg',
  //   desc: 'Pizza Especial Sabor Depressão',
  //   price: 18.90,
  //   type: 1,
  //   quantity: 0
  // },
  // {

  //   nome: 'Fanta',
  //   image: '../../images/produto2.jpg',
  //   desc: 'Essa coca é fanta',
  //   price: 18.90,
  //   type: 2,
  //   quantity: 0
  // },
  // {
  //   nome: 'REdbull',
  //   image: '../../images/produto1.jpg',
  //   desc: 'Fantinha',
  //   price: 18.90,
  //   type: 2,
  //   quantity: 0
  // },
  ];

  const checkout = () => {
    let aux = [];
    data.map((item) => {
      if (item.quantity > 0) {
        aux.push(item);
      }
    });

    localStorage.setItem("itens", JSON.stringify(aux));
    localStorage.setItem("total", total);

    history.push(`/checkout`);
  };

  const [place, setPlace] = useState();
  const [data, setData] = useState([]);
  const [dataFilter, setDataFilter] = useState([]);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    let slug = props.match.params.slug;
    console.log(slug);
    // Api.get("/estabelecimento/" + slug + "/produtos").then((res) => {
    //   setData(res.data.products);
    //   setDataFilter(res.data.products);
    //   setPlace(res.data);
    // });

    setData(values);
    setDataFilter(values);

    window.scroll({ top: 0, left: 0, behavior: "smooth" });
  }, []);

  const addItem = (event, id) => {
    console.log(event);
    let aux = event[id];
    if (aux.quantity) {
      aux.quantity++;
    } else {
      aux.quantity = 1;
    }
    setData(event);

    setTotal(total + aux.price);
  };

  const removeItem = (event, id) => {
    let aux = event[id];
    aux.quantity--;
    setData(event);
    if (total > 0) {
      setTotal(total - aux.price);
    }
  };

  // useEffect(() => {
  //     setData(dataFilter);
  // }, [dataFilter])

  function filterData(type) {
    let aux = [];
    if (type === 0 || type === -1) {
      setData(dataFilter);
    } else {
      dataFilter.map((item) => {
        if (item.type === type) {
          aux.push(item);
        }
      });
      if (aux !== null) {
        setData(aux);
      }
    }
  }

  return (
    <>
      <div id="main-banner" className="container-fluid banner-place">
        <div id="banner-content" className="container">
          <div
            id="banner-logo"
            className="shadow"
            style={{ backgroundImage: `url(../../images/place2.jpg)` }}
          ></div>
          <div id="banner-infos">
            <h2>Rock Burguer</h2>
            <p>O melhor hamburguer da cidade!</p>
          </div>
        </div>
      </div>

      <div id="nav-breadcrumb">
        <div className="container-fluid">
          <span>
            <a href="index.html">
              <i className="fas fa-chevron-left"></i>
            </a>
            Rock Burguer
          </span>
        </div>
      </div>

      <div id="main-bar">
        <div className="container row">
          <div id="input-search" className="col-md-7 shadow-sm">
            <input
              type="text"
              className="form-control"
              id="main-bar-search"
              placeholder="Busque aqui algum produto..."
            />
            <button type="submit">
              <i className="fas fa-search"></i>
            </button>
          </div>
          <div className="select-category col-md-3">
            <select
              className="form-control"
              id="filterData"
              onChange={(e) => filterData(Number(e.target.value))}
            >
              <option value={0}> Selecione a categoria</option>
              <option value={1}> Comidas</option>
              <option value={2}> Bebidas</option>
            </select>
          </div>
        </div>
      </div>

      <div id="near-places" className="container">
        <h2>Produtos</h2>
        <div className="places-list">
          <div className="place-list-row row">
            {data.map((item, index) => (
              <div key={item.id} className="col-md-3">
                <div className="item-card">
                  <div id="clickerSpace" onClick={(e) => addItem(data, index)}>
                    {item.quantity > 0 ? (
                      <div className="item-qtt shadow-sm">
                        <span>{item.quantity}</span>
                      </div>
                    ) : (
                        ""
                      )}
                    <div
                      className="item-img"
                      style={{
                        backgroundImage: `url(${item.image})`,
                        cursor: "pointer",
                      }}
                    ></div>
                  </div>
                  <div className="item-footer">
                    <span className="item-name">{item.name}</span>
                    <span className="item-description">{item.desc}</span>
                    <span className="item-price">
                      {" "}
                      {item.price
                        ? item.price.toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        : "R$ 0,00"}
                    </span>
                    <button
                      className="item-add shadow-sm"
                      v-if="place.location"
                      onClick={(e) => addItem(data, index)}
                    >
                      <i className="fas fa-plus"></i>
                    </button>
                    {item.quantity > 0 ? (
                      <button
                        className="item-add shadow-sm"
                        v-if="place.location"
                        onClick={(e) => removeItem(data, index)}
                      >
                        <i className="fas fa-minus"></i>
                      </button>
                    ) : (
                        <button
                          className="item-add shadow-sm"
                          v-if="place.location"
                          disabled
                        >
                          <i className="fas fa-minus"></i>
                        </button>
                      )}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>

        <button className="btn btn-fixed" onClick={checkout} id="more-products">
          Finalizar compra -{" "}
          {total
            ? total.toLocaleString("pt-BR", {
              style: "currency",
              currency: "BRL",
            })
            : "R$ 0,00"}
        </button>
      </div>
    </>
  );
};

export default Place;
