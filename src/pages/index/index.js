import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import Api from "../../services/api";

const Index = () => {

  const values = [
    {
      categories: [{
        id: 5,
        name: "bebidas",
        pivot: { category_id: 5, event_id: 86, }
      }],
      id: 1,
      locations: [{ id: 62, city: "simCity" }],
      name: "247 Bebidas",
      slug: "arf-slug",
      thumb: "../../images/place1.jpg",
    },
    {
      categories: [{
        id: 4,
        name: "restaurantes",
        pivot: { category_id: 4, event_id: 86, }
      }],
      id: 2,
      locations: [{ id: 62, city: "Vitória" }],
      name: "Folha Foods",
      slug: "arf-slug",
      thumb: "../../images/place3.jpg",
    },
    {
      categories: [{
        id: 4,
        name: "restaurantes",
        pivot: { category_id: 4, event_id: 86, }
      }],
      id: 3,
      locations: [{ id: 62, city: "simCity" }],
      name: "Rock Burguer",
      slug: "arf-slug",
      thumb: "../../images/place2.jpg",
    },
    {
      categories: [{
        id: 5,
        name: "bebidas",
        pivot: { category_id: 5, event_id: 86, }
      }],
      id: 4,
      locations: [{ id: 62, city: "simCity" }],
      name: "Lider Distribuidora",
      slug: "arf-slug",
      thumb: "../../images/place4.jpg",
    },
  ];

  const cat = [
    { id: 4, name: "restaurantes", image: "http://18.228.49.63/uploads/food/category1.jpg" },
    { id: 5, name: "bebidas", image: "http://18.228.49.63/uploads/food/category2.jpg" },
    { id: 6, name: "mercado", image: "http://18.228.49.63/uploads/food/category3.jpg" },
    { id: 7, name: "padaria", image: "http://18.228.49.63/uploads/food/category4.jpg" },
    { id: 8, name: "açougue", image: "http://18.228.49.63/uploads/food/category5.jpg" },
  ]

  const city = [
     "simCity", "Vitória"
  ]

  const [data, setData] = useState([]);
  const [dataFilter, setDataFilter] = useState([]);
  const [categorias, setCategorias] = useState([]);
  const [cidades, setCidades] = useState([]);

  useEffect(() => {
    // Api.get("/categorias").then((res) => {
    //   console.log(res.data);
    //   setCategorias(res.data);
    // });

    Api.get("/cidades/estabelecimentos").then((res) => {
      console.log(res.data);
      setCidades(res.data);
    });

    // Api.get("/estabelecimentos").then((res) => {
    //   // setData(res.data);
    //   // setDataFilter(res.data);
    // });
    setCategorias(cat);
    setData(values);
    setDataFilter(values);

  }, []);

  function filterData(type) {
    let aux = [];

    if (type === 0 || type === -1) {
      setData(dataFilter);
    } else {
      dataFilter.map((item) => {
        if (item.categories[0]?.id === type) {
          aux.push(item);
        }
      });
      if (aux !== null) {
        setData(aux);
      }
    }
  }

  function filterPlace(type) {
    let aux = [];

    if (type === "0" || type === "-1") {
      setData(dataFilter);
    } else {
      dataFilter.map((item) => {
        if (item.locations[0]?.city === type) {
          aux.push(item);
        }
      });
      if (aux !== null) {
        setData(aux);
      }
    }
  }

  function filterName(name) {
    let aux = [];
    if (name === "" || name === undefined) {
      setData(dataFilter);
    } else {
      dataFilter.map((item) => {
        if (item.name.includes(name)) {
          aux.push(item);
        }
      });
      if (aux !== null) {
        setData(aux);
      }
    }
  }

  return (
    <>
      <div id="main-banner" className="container-fluid banner-home">
        <div id="banner-content" className="container banner-home">
          <div id="banner-infos">
            <h2>Descubra estabelecimentos e faça seus pedidos.</h2>
            <p>
              Encontre aquele local preferido sem sair de casa, faça seu
              carrinho de compras, peça e receba em seu endereço!
            </p>
            <div id="input-search" className="shadow-sm">
              <input
                type="text"
                className="form-control"
                id="main-bar-search"
                placeholder="Digite aqui o estabelecimento"
              />
              <button type="submit">
                <i className="fas fa-search"></i>
              </button>
            </div>
          </div>
          <div
            id="banner-img"
            style={{ backgroundImage: `url(./images/banner-img.png)` }}
          ></div>
        </div>
      </div>

      <div id="header-categories">
        <div className="container ">
          {categorias.map((categoria) => (
            <a key={categoria.id} href="javascrpit.void(0)">
              <div className="category-item">
                <div className="category-img shadow-sm">
                  <img alt={categoria.name} src={categoria.image} />
                </div>
                <span>{categoria.name}</span>
              </div>
            </a>
          ))}
        </div>
      </div>

      <div id="main-bar">
        <div className="container row">
          <div id="input-search" className="shadow-sm col-md-6">
            <input
              type="text"
              className="form-control"
              id="main-bar-search"
              placeholder="Digite aqui o estabelecimento"
              onChange={(event) => filterName(event.target.value)}
            />
            <button type="submit">
              <i className="fas fa-search"></i>
            </button>
          </div>
          <div className="select-category col-md-3">
            <select
              className="form-control"
              id="filterPlace"
              onChange={(e) => filterPlace(e.target.value)}
            >
              <option value={"-1"}> Selecione a Cidade</option>
              <option value={"0"}>Todas</option>
              {cidades.map((cidade, index) => (
                <option key={index} value={cidade}>
                  {cidade.charAt(0).toUpperCase() + cidade.slice(1)}
                </option>
              ))}
            </select>
          </div>
          <div className="select-category col-md-3">
            <select
              className="form-control"
              id="filterData"
              onChange={(e) => filterData(Number(e.target.value))}
            >
              <option value={-1}>Selecione a categoria</option>
              <option value={0}>Todas</option>
              {categorias.map((categoria) => (
                <option key={categoria.id} value={categoria.id}>
                  {categoria.name.charAt(0).toUpperCase() +
                    categoria.name.slice(1)}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>

      <div id="near-places" className="container">
        <h2>Novos estabelecimentos.</h2>
        <div className="places-list">
          <div className="place-list-row">
            {data.map((item) => (
              <NavLink key={item.id} to={"/place/" + item.slug}>
                <div className="place-card">
                  <div
                    className="place-img"
                    style={{
                      backgroundImage: `url(${item.thumb})`,
                      cursor: "pointer",
                    }}
                  ></div>
                  <span className="place-tag shadow-sm">
                    {item.categories[0]?.name}
                  </span>
                  <div className="place-footer">
                    <span className="place-name">{item.name}</span>
                    <span className="place-location" v-if="place.location">
                      <i className="fas fa-map-marker-alt"></i>
                      {item.locations[0]?.city}
                    </span>
                  </div>
                </div>
              </NavLink>
            ))}
          </div>
        </div>

        <button className="btn" id="more-places">
          ver mais produtos
        </button>
      </div>
    </>
  );
};

export default Index;
